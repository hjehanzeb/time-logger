from django.urls import path
from django.conf.urls import url, include

from . import views
from . import apis
from rest_framework.urlpatterns import format_suffix_patterns
from .api.views import CreateViewTimeLog, CreateViewTimeRecord, \
    DetailsViewTimeLog, DetailsViewTimeRecord

from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    # ex: /logtime/
    path('', views.index, name='index'),
    # ex: /logtime/daily/1/1/2009
    path('daily/<int:day>/<int:month>/<int:year>/', views.daily, name='daily'),
    # ex: /logtime/daily/
    path('daily/', views.daily, name='daily'),
    # # ex: /logtime/5/results/
    # path('<int:question_id>/results/', views.results, name='results'),
    # # ex: /logtime/5/vote/
    # path('<int:question_id>/vote/', views.vote, name='vote'),
    # ex: /logtime/api/add-entry
    # path('api/add-entry/', apis.add_entry, name='add-entry'),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/timelogs$', CreateViewTimeLog.as_view(), name="create-timelog"),
    url(r'^api/timelogs/(?P<pk>[0-9]+)/$', DetailsViewTimeLog.as_view(), name="details-timelog"),
    url(r'^api/timerecords$', CreateViewTimeRecord.as_view(), name="create-timerecord"),
    url(r'^api/timerecords/(?P<pk>[0-9]+)/$', DetailsViewTimeRecord.as_view(), name="details-timerecord"),
    url(r'^api/get-token/', obtain_auth_token)
]

urlpatterns = format_suffix_patterns(urlpatterns)
