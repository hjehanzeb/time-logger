"""
Defines the custom filters that can be used in templates.

Variables:
    register {template.Library} -- Main object for registering template filters
"""
from django import template
from datetime import timedelta
# from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter(name='roundit')
# @stringfilter
def roundit(value):
    """Round the float value to 2 decimal points."""
    return round(value, 2)

# register.filter('cut', cut)


@register.filter(name='gettimeview')
def gettimeview(secs):
    """To convert seconds in to HH:mm format."""
    return ":".join(
        str(
            timedelta(
                seconds=int(secs)
            )
        ).split(":")[0:2]
    )


@register.filter(name='get_day_total')
def get_day_total(week_dictionary, today):
    """hello."""
    return week_dictionary[today.strftime('%d-%m-%Y')].total_time_sec
