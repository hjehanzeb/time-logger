"""
View file for APIs.

This model will contain all the APIs for the daily page or other pages
"""
from logtime.models import TimeRecord, Project, TaskType, TimeLog
from datetime import datetime
from django.http import JsonResponse


def add_entry(request):
    """
    API to add time entry.

    Arguments:
        request {HttpRequest} -- django.http.HttpRequest object

    Returns:
        HttpResponse -- The string response to be sent to the client
    """
    # Getting the project and task_type object from database to be linked to Time record
    project_load = Project.objects.get(pk=request.GET.projectId)
    task_type_load = TaskType.objects.get(pk=request.GET.taskTypeId)

    # Create Time record object
    time_record = TimeRecord.objects.create(
        project=project_load,
        task_type=task_type_load,
        summary=request.GET.summary,
        total_time_in_sec=request.GET.secElapsed,
        is_running=request.GET.isRunning
    )

    # Time Log object, with above time record linked to it
    TimeLog.objects.create(
        time=time_record,
        start_time=datetime.strptime(request.GET.startTime, '%Y-%m-%d %H:%M:%S'),
        end_time=datetime.strptime(request.GET.stopTime, '%Y-%m-%d %H:%M:%S'),
    )

    # Return json to tell that record has been successfully saved
    return JsonResponse({'response': 'success', 'id': time_record.id})


def update_entry(request):
    """
    To update a time entry if it already exists.

    Arguments:
        request {HttpRequest} -- Object containing request variables
    """
    time_record_load = Project.objects.get(pk=request.GET.timeRecordId)
    time_record_load.total_time_in_sec = time_record_load.total_time_in_sec + request.GET.secElapsed

    # Create new time log entry with time time record
    TimeLog.objects.create(
        time=time_record_load,
        start_time=datetime.strptime(request.GET.startTime, '%Y-%m-%d %H:%M:%S'),
        end_time=datetime.strptime(request.GET.stopTime, '%Y-%m-%d %H:%M:%S'),
    )
