from django.db import models

# For Authorization Token
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.dispatch import receiver


class Client(models.Model):
    """Object to store clients."""

    name = models.CharField(max_length=100)
    address = models.TextField()
    preferred_currency = models.CharField(max_length=10)
    created_date = models.DateTimeField(auto_now=True)

    # Methods
    def __str__(self):
        """Return default string for this class."""
        return self.name


class Project(models.Model):
    """To store the Projects."""

    name = models.CharField(max_length=100)
    client = models.ForeignKey(Client, default=None, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=100)
    project_key = models.CharField(max_length=5)
    created_date = models.DateTimeField(auto_now=True)

    # Methods
    def __str__(self):
        """Return default string for this class."""
        return self.name


class TaskType(models.Model):
    """Task type to hold the types a task can be assigned."""

    name = models.CharField(max_length=100)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    billable = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return default string for this class."""
        return self.name + " (Billable: " + str(self.billable) + ")"


class TaskTypeFixed(models.Model):
    """Containing the list of task types that are fixed for every project."""

    name = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now=True)


class TimeRecord(models.Model):
    """The table that will store time records per day."""

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    task_type = models.ForeignKey(TaskType, on_delete=models.CASCADE, null=True)
    summary = models.TextField()
    total_time_in_sec = models.IntegerField(default=0)
    is_running = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now=True)

    # Methods
    def __str__(self):
        """
        Get the necessary details from the object.

        Returns:
            String -- The string to be returned
        """
        return self.summary + " " + str(round(self.total_time_in_sec / 60 / 60, 2)) + "h"


class TimeLog(models.Model):
    """It will store logs, or basically log each chunk that time record is based on."""

    time = models.ForeignKey(TimeRecord, on_delete=models.CASCADE)
    start_time = models.TimeField()
    end_time = models.TimeField()
    created_date = models.DateTimeField(auto_now=True)

    # Methods
    def __str__(self):
        """
        Get the necessary details from the object.

        Returns:
            String -- The string to be returned
        """
        return str(self.start_time) + "-" + str(self.end_time)


# This receiver handles token creation immediately a new user is created.
@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """Create Auth token on the creation of a user."""
    if created:
        Token.objects.create(user=instance)
