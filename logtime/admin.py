from django.contrib import admin

# Register your models here.
from .models import Project, TimeRecord, TimeLog, Client

admin.site.register(Project)
admin.site.register(TimeRecord)
admin.site.register(TimeLog)
admin.site.register(Client)
