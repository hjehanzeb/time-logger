class LogTimeModal
{
    /**
     * Open a modal dialog window based on the elementId that's passed as an argument
     *
     * @param  {string} elementId Element's identification string
     * @return {void}
     */
    static openNew(elementId) {
        // alert('New Modal window to be opened here');
        $(elementId).iziModal('open');
    }

    static attachTo(elementId, callback) {
        $(elementId).iziModal({iframeHeight: 400});
        $(document).on('opened', elementId, callback);
    }

    static close(elementId) {
        $(elementId).iziModal('close');
    }
}
