/**
 * Timer class for initiating timer on a page
 */
class Timer
{
    /**
     * Constructor of this class
     *
     * @param  {string} timerStartValue Should be in the format 00:00
     * @return {void}
     */
    constructor(timerStartValue) {
        let typeOfArg = (timerStartValue === null) ? 'number' : typeof timerStartValue;
        let functionName = 'constructor' + typeOfArg.charAt(0).toUpperCase() + typeOfArg.slice(1);
        this[functionName].call(this, timerStartValue);
    }

    /**
     * Based on the type of argument, the particular construction will be called.
     * This constructor is for argument of type Number
     *
     * @param  {Number} timerStartValue Starging value of the timer
     * @return {void}
     */
    constructorNumber(timerStartValue) {
        this.secElapsed = 0;
        let timeSplit = ["00", "00"];

        // If a time is already up to some value, then secElapsed needs to be started
        // from that time
        if (timerStartValue !== null) {
            this.secElapsed = timerStartValue;
            timeSplit = Timer.convertSecsToTime(timerStartValue).split(":");
        }
        this.rowAttached = null;
        this.timerId = 'timer-' + Math.random().toString().split('.')[1];

        // <div> element DOMObject
        this.element = $('<div id="'+ this.timerId +'" class="hidden timer-container" hidden><span class="hours">' + timeSplit[0] + '</span><span class="minutes">' + timeSplit[1] + '</span><span class="seconds">00</span></div>');

        // Appending the <div> element to the <body> tag
        $('body').append(this.element);
    }

    /**
     * Based on the type of argument, the particular construction will be called.
     * This constructor is for argument of type Object of Timer itself
     *
     * @param  {Timer} jsObjectForTimer Timer object that contains the initial values for this contructor
     * @return {void}
     */
    constructorObject(jsObjectForTimer) {
        this.secElapsed = jsObjectForTimer.secElapsed;
        this.rowAttached = jsObjectForTimer.rowAttached;
        this.timerId = jsObjectForTimer.timerId;
        this.element = jsObjectForTimer.element;

        // Appending the <div> element to the <body> tag
        $('body').append(this.element);
    }

    /**
     * To start the timer by adding a <div> in DOM and attach 1 sec interval to this timer class
     *
     * @return {void}
     */
    start() {
        // Adding an interval of 1 sec to update secElapsed and the <div> added for this timer
        let self = this;
        this.timerInterval = setInterval(function () {
            self.secElapsed += 1;
            self.updateElement();
        }, 1000);

        // Recording the browser time now, which will be used to update the actual time. Reason is because
        // setInterval sometimes lags and doesn't record correct time
        this.timeStart = new Date();
    }

    /**
     * Stop the timer, clear the interval and secElapsed to 0
     *
     * @return {void}
     */
    stop() {
        clearInterval(this.timerInterval);
        this.secElapsed = 0;
        this.timeEnd = new Date();
    }

    /**
     * Pause the timer, only clears the interval and doesn't clear secElapsed property
     *
     * @return {void}
     */
    pause() {
        clearInterval(this.timerInterval);
    }

    /**
     * Update the timer <div> element with hours and minutes and seconds separately in the
     * format hh:mm:ss
     *
     * @return {void}
     */
    updateElement() {
        let timeSplitted = Timer.convertSecsToTime(this.secElapsed).split(":");

        // Calculating the hours and minutes 
        let hours = timeSplitted[0];
        let minutes = timeSplitted[1];
        let secs = timeSplitted[2];

        // Updating the <div> associated with this timer
        $('#' + this.timerId).children('.hours').html(hours);
        $('#' + this.timerId).children('.minutes').html(minutes);
        $('#' + this.timerId).children('.seconds').html(secs);

        // If there is some TimeRecord row attached to this timer, update it as well and the DOM
        // associated with it
        if (this.rowAttached !== null) {
            this.rowAttached.children('.entry-time').html(hours + ":" + minutes);
        }
    }

    /**
     * Attach a TimeRecord with this timer
     *
     * @param  {DOMObject} $rowEle The TimeRecord DOM element that needs to be updated with this timer
     * @return {void}
     */
    updateTimeSheetRow($rowEle) {
        this.rowAttached = $rowEle;
    }

    /**
     * A helper function, that should be in the Helper class, to prepend 0 if value is less than 10
     *
     * @param  {int}    n The number to which 0 needs to be prepended
     * @return {string}   The number with 0 prepended if needed, but it will be a string now
     */
    static addZero(n) {
        return n < 10 ? ('0' + n) : ('' + n);
    }

    /**
     * Convert the time in the format 00:00 to seconds
     *
     * @param  {string} time Time in format 00:00
     * @return {int}         Time calculated in seconds
     */
    static convertTimeToSecs(time) {
        if (time !== null) {
            let timeSplit = time.split(':');
            return 
                parseInt(timeSplit[0]) * 60 * 60
                + parseInt(timeSplit[1]) * 60
                + (timeSplit.length > 2 ? parseInt(timeSplit[2]) : 0)
        }

        return null;
    }

    /**
     * Convert the time in secs to the format hh:mm:ss
     *
     * @param  {int}    secs The seconds elapsed
     * @return {string}      Time in the format hh:mm:ss
     */
    static convertSecsToTime(secs) {
        let hours = 0;
        let minutes = 0;
        if (secs !== null) {
            // Calculating the hours and minutes 
            hours = Math.floor(secs / 60 / 60);
            minutes = Timer.addZero(Math.floor((secs - hours * 60 * 60) / 60));
            secs = Timer.addZero(Math.floor((secs - hours * 60 * 60 - minutes * 60)));
        }

        return hours + ":" + minutes + ":" + secs;
    }

    /**
     * Stop the timer, clear the interval and secElapsed to 0, and remove the <div> element
     * attached to this object
     *
     * @return {void}
     */
    destroyTimer() {
        this.stop();
        $('#' + this.timerId).remove();
    }
}
