/**
 * Each daily record pages exists of one or multiple entries of TimeRecord objects each holding the record
 * from the records per day
 */
class TimeRecord
{
    constructor (startTime, summary, taskType, index) {
        this.timeElapsed = Timer.convertTimeToSecs(startTime);
        this.timerObject = null;
        this.timer = null;
        this.summary = summary;
        this.taskType = taskType;
        this.projectName = null;
        this.projectKey = null;
        this.clientName = null
        this.rowEntry = $('<tr class="timesheet-entry-row"><td><div class="entry-info"><div class="project-client"><span class="project"> </span> <span class="client"></span></div><div class="task-notes"><span class="task">Programming</span> <span class="ndash">–</span><p><span class="notes"></span></p></div></div></td><td class="entry-time"></td><td class="entry-button do-not-print"><button class="hui-button hui-button-large js-start-timer start-stop-timer" id="start-button-937683817" type="button"><svg class="icon-timer" height="22" viewbox="0 0 22 22" width="22"><circle class="icon-timer-face" cx="11" cy="11" r="10" stroke-width="2"></circle><path class="icon-timer-hand" d="M12.8 10.2L11 2l-1.8 8.2-.2.8c0 1 1 2 2 2s2-1 2-2c0-.3 0-.6-.2-.8z"></path></svg> Start</button></td><td class="edit-button do-not-print"><button class="hui-button hui-button-small button-icon-only js-edit-entry" title="Edit Entry" type="button"><i data-icon="edit"></i></button></td></tr><tr id="day_entry_replacement"></tr>');
        this.index = index;
        this.classObj = null;
    }

    /**
     * Setting variable/attribute of this class
     * @param  {string} variable The name of the variable
     * @param  {mixed}  value    The value for that variable
     * @return {void}            Just sets the variable value and doesn't return anything
     */
    setter(variable, value) {
        this[variable] = value;
    }

    setTimer() {
        // Start the timer
        this.timer = new Timer(this.timeElapsed);
        this.timer.start();

        // Ask timer to update this timesheet row
        if (this.classObj !== null) {
            this.timer.updateTimeSheetRow(this.classObj);
        }
    }

    stopTime() {
        this.timeElapsed = this.timer.secElapsed;
        this.timer.stop();
    }

    destroyTimer() {
        this.timer.destroyTimer();
    }

    /**
     * Render HTML of TimerRecord object
     *
     * @return {void}
     */
    render() {
        // Create an entry in the time logs in the page, and add that timer to it
        this.classObj = this.rowEntry.clone();

        this.classObj.attr('data-index', this.index);       // Add index to the timer row being created
        this.classObj.find('.start-stop-timer').addClass('timer-started');  // Add class to the Start/Stop button
        this.classObj.children('.entry-time').html('');     // Set time

        // Appending the timer element at the last of the array, if none other exists
        $('#day_entry_replacement').replaceWith(this.classObj);

        // Set the description of the task
        this.classObj.find('.task-notes > p > .notes').html(this.summary);

        // Set Project name and key
        this.classObj.find('.project-client > .project').html('[' + this.projectKey + '] ' + this.projectName +
            ' (' + this.clientName + ')');
    }
}
