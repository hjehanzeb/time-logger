/**
 * All page events etc and handling other functions are doing in this class
 */
class LogTimeDaily
{
    /**
     * Construction function for defining button objects and attaching events
     */
    constructor() {
        /**
         * Timer is to store the Timer object of the daily page
         * @type {Timer}
         */
        this.timer = null;

        /**
         * Stores the indices of the TimeRecord objects or the records on daily page
         * @type {Number}
         */
        this.indices = -1;

        /**
         * This variable stores the TimeRecord objects in an array for all the records in a page
         * @type {Array}
         */
        this.timeRecords = [];

        var self = this;
        if ($('.timesheet-entry-row').length > 0) {
            this.indices = $('.timesheet-entry-row').length - 1;

            // Attaching TimeRecord objects to each row
            $('.timesheet-entry-row').each(function (index, object) {
                let rowRecord = new TimeRecord(
                    $('.timesheet-entry-row[data-index="' + index + '"]').find('.entry-time').text(),
                    $('.timesheet-entry-row[data-index="' + index + '"]').find('.task-notes > p > .notes').text(),
                    null,
                    index
                );
                self.timeRecords[index] = rowRecord;
                rowRecord = null;
            });
        }
        this.defineButtonObjects();

        // Attaching some events
        this.newEntryButton.click(this.newEntryHandler.bind(this));
        LogTimeModal.attachTo('.modal-time-entry', function (e) {
            console.log('inside openNew: ', $('.close-new-entry-modal'));
            self.modalCancelButton = $('.close-new-entry-modal');
            self.modalCancelButton.click(self.closeModal.bind(self));
            self.modalStartButton = $('.start-timer-button');
            self.modalStartButton.click(self.startTimer.bind(self));
        });

        // The event attachment to Start/Stop button against each timer row
        $(document).on('click', '.start-stop-timer', this.clickStartStopButton.bind(this));
        // this.modalCancelButton.click(this.closeModal);
        
        // On navigate away, store the timer
        window.addEventListener('beforeunload', function (e) {
            // Cancel the event
            // e.preventDefault();
            // Chrome requires returnValue to be set
            // e.returnValue = 'unloading event';
            console.log('unloading');
            self.updatePageTimer(self.timer);
        });
        
        // On load, check if there is anything in localstorage
        $(document).ready(this.loadLocalStorageTimer.bind(this));
    }

    /**
     * Defining some class properties for buttons that will be used later on in the class
     *
     * @return {void}
     */
    defineButtonObjects() {
        // Defining Button objects
        this.newEntryButton = $('.button-new-time-entry');
        // this.modalCancelButton = $('.close-new-entry-modal');
    }

    /**
     * Open the New Entry model box
     *
     * @return {void}
     */
    newEntryHandler() {
        LogTimeModal.openNew('.modal-time-entry');
    }

    /**
     * To start the stopped timer or a new timer, when needed
     *
     * @return {void}
     */
    startTimer() {
        var timerStart = null;

        // Remove if there is any old timer
        if (this.timer !== null) {
            this.timer.destroyTimer();
            this.updatePageTimer(null);
        }

        // Adding the time record
        // See if there is any time inputted in the box, we need to start from that time
        if ($('.modal-time-entry [name="hours"]').val() !== "") {
            timerStart = $('.modal-time-entry [name="hours"]').val();
        }
        this.setRowRecord(timerStart, {
            projectName: $('[name="project_id"]').find('option:selected').text().trim(),
            projectKey: $('[name="project_id"]').find('option:selected').data('key'),
            clientName: $('[name="project_id"]').find('option:selected').parent('optgroup').attr('label')
        });

        this.closeModal();
    }

    /**
     * When the Start / Stop button is clicked, this event handler is called
     *
     * @param  {eventObject} e The HTML Dom click event object for the Start / Stop button
     * @return {void}          Doesn't return anything
     */
    clickStartStopButton(e) {
        var pageObj = this;
        console.log('e.targetEvent: ', e);
        if (typeof $(e.currentTarget).parents('.timesheet-entry-row').data('index') != 'undefined') {
            let index = $(e.currentTarget).parents('.timesheet-entry-row').data('index');

            // If Timer has already started, then we need to stop it, else if it's stopped, then we need to start it
            if ($(e.currentTarget).hasClass('timer-started')) {
                $(e.currentTarget).removeClass('timer-started');
                pageObj.timeRecords[index].timeElapsed = pageObj.timeRecords[index].timer.secElapsed;
                pageObj.timeRecords[index].destroyTimer();
                pageObj.updatePageTimer(null);
            } else {
                $(e.currentTarget).addClass('timer-started');
                pageObj.timeRecords[index].setter('classObj', $(e.currentTarget).parents('.timesheet-entry-row'));
                pageObj.timeRecords[index].setTimer();
                pageObj.updatePageTimer(pageObj.timeRecords[index].timer);
            }
        }
    }

    /**
     * Store the Timer object passed in argument to localStorage
     *
     * @param  {Timer} timerObject The timer object which needs to be stored
     * @return {void}
     */
    updatePageTimer(timerObject) {
        this.timer = timerObject;

        // If timerObject is not null, we need to store it in localStorage so that we get it if a page is reloaded
        if (timerObject !== null) {
            timerObject.date = $('.day-view-table').data('timesheet-date');
            localStorage.setItem('timer_logtime_daily_current_timer', JSON.stringify(timerObject));
        } else {
            // localStorage.removeItem('timer_logtime_daily_current_timer');
        }
    }

    /**
     * Close the modal window and unset events attached to its buttons
     */
    closeModal() {
        LogTimeModal.close('.modal-time-entry');

        // Unset the events
        this.modalCancelButton.off('click');
        this.modalStartButton.off('click');
    }

    /**
     * Load timer from localstorage and set the active timer as running
     *
     * @return {void}
     */
    loadLocalStorageTimer() {
        let localstoreTimerJson = localStorage.getItem('timer_logtime_daily_current_timer');
        if (localstoreTimerJson !== null) {
            // If there is, let's check if we are on todays timer. Based on that we will restart timer for active
            // date based on the settings stored in local storage
            // if date in localstoreTimerJson is equal to date of page, we have what we wanted,
            // start that pages timer ;)
            let pageDate = new Date($('.day-view-table').data('timesheet-date'));  // Date object of Page's date
            let localstoreTimer = JSON.parse(localstoreTimerJson);      // LocalStorage's timer parsed to object
            let storedTimerDate = new Date(localstoreTimer.date);       // Getting date from localStorage's timer obj

            // If both dates are same, then timer belongs to this page. Restart the timer's entry
            // else we don't need to do anything
            if (storedTimerDate.getTime() == pageDate.getTime()) {
                this.setRowRecord(Timer.convertSecsToTime(localstoreTimer.secElapsed), {
                    timeElapsed: localstoreTimer.secElapsed
                });
                // this.timer = new Timer(localstoreTimer.secElapsed);
                // this.timer.start();
            }
        }
    }

    /**
     * Create a record row with timer in it, set to the value of 'timerStart'
     *
     * @param {Number} timerStart The value at which the timer will start in the record row
     * @param {Object} params     Key value pair, containing values of attributes of TimeRecord class
     */
    setRowRecord(timerStart, params) {
        let rowRecord = new TimeRecord(timerStart, $('.modal-time-entry .entry-notes').val(), null, this.indices + 1);

        // Setting class variables coming in parameters
        for (let [key, value] of Object.entries(params)) {
            rowRecord.setter(key, value);
        }

        // Produces a row
        rowRecord.render();
        this.timeRecords[this.indices + 1] = rowRecord;

        // Set a new timer and assign it to this timer record
        rowRecord.setTimer();
        this.updatePageTimer(rowRecord.timer);
    }

    _bind(func, context) {
        if (func.bind === FuncProto.bind && FuncProto.bind)
            return FuncProto.bind.apply(func, slice.call(arguments, 1));
        var args = slice.call(arguments, 2);
        return function() {
            return func.apply(context, args.concat(slice.call(arguments)));
        };
    }
}
let logTimeDailyObj = new LogTimeDaily();
