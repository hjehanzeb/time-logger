"""Views classes for APIs."""
from rest_framework import generics
from .serializers import TimelogSerializer, TimerecordSerializer
from ..models import TimeLog, TimeRecord
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView


class CreateViewTimeLog(generics.ListCreateAPIView):
    """This class defines the create behavior of our rest api."""

    queryset = TimeLog.objects.all()
    serializer_class = TimelogSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new timelog entry."""
        serializer.save()


class DetailsViewTimeLog(generics.RetrieveUpdateDestroyAPIView):
    """This class handles the http GET, PUT and DELETE requests."""

    queryset = TimeLog.objects.all()
    serializer_class = TimelogSerializer


class CreateViewTimeRecord(generics.ListCreateAPIView):
    """docstring for CreateViewTimeRecord."""

    queryset = TimeRecord.objects.all()
    serializer_class = TimerecordSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new timelog entry."""
        serializer.save()


class DetailsViewTimeRecord(generics.RetrieveUpdateDestroyAPIView):
    """This class handles the http GET, PUT and DELETE requests."""

    queryset = TimeRecord.objects.all()
    serializer_class = TimerecordSerializer
