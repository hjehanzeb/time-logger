from rest_framework import serializers
from ..models import TimeLog, TimeRecord


class TimelogSerializer(serializers.ModelSerializer):
    """Serializer to map the TimeLog Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""

        model = TimeLog
        fields = ('time', 'start_time', 'end_time', 'created_date')
        read_only_fields = ['created_date']


class TimerecordSerializer(serializers.ModelSerializer):
    """Serializer to map the TimeRecord Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""

        model = TimeRecord
        fields = ('project', 'task_type', 'summary', 'total_time_in_sec', 'is_running', 'created_date')
        read_only_fields = ['created_date']
