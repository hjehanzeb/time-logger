from django.apps import AppConfig


class LogtimeConfig(AppConfig):
    name = 'logtime'
