from django.shortcuts import render, redirect
from django.http import Http404
# from django.utils import timezone
from datetime import datetime
from datetime import date
from datetime import timedelta
from logtime.models import TimeRecord, Project
from django.db.models import Func, F, Sum, Min
from operator import itemgetter
from logtime.helper import helpers
from collections import namedtuple


# Create your views here.
def daily(request, day=None, month=None, year=None):
    """[summary].

    [description]

    Arguments:
        request {[type]} -- [description]

    Keyword Arguments:
        day {[type]} -- [description] (default: {None})
        month {[type]} -- [description] (default: {None})
        year {[type]} -- [description] (default: {None})

    Returns:
        [type] -- [description]
    """
    new_weekly_dictionary = {}
    CustomTimeRecord = namedtuple("CustomTimeRecord", "created_date_only total_time_sec")

    if day is None:
        date_obj = datetime.now()
        today = date_obj.day
        month = date_obj.month
        year = date_obj.year
    else:
        today = day
        date_obj = date(year, month, today)

    fh = open('/home/haseeb.jehanzeb/.virtualenvs/timer/timer/logtime/logs.log', 'a')
    # Enclosing everything in the try...catch block
    try:
        fh.write(str(today) + "-" + str(month) + "-" + str(year) + "\n")
        # Getting the time record list for the date given
        records_list = TimeRecord.objects.filter(created_date__day=today) \
            .filter(created_date__month=month) \
            .filter(created_date__year=year)
        fh.write("record list: " + str(records_list) + "\n")

        # Getting project list for showing in new entry dialog
        projects = Project.objects.all()
        fh.write("projects: " + str(projects) + "\n")

        # Getting Week's records and their total time for the current date
        # weeks_records = TimeRecord.objects \
        #     .annotate(total_time_sec=Sum('total_time_in_sec')) \
        #     .filter(created_date__week=date_obj.isocalendar()[1]) \
        #     .values('total_time_sec', 'created_date__date')
        weeks_records = TimeRecord.objects.raw("SELECT id, DATE(`logtime_timerecord`.`created_date`) as created_date_only, SUM(`total_time_in_sec`) as total_time_sec FROM `logtime_timerecord` WHERE WEEK(`logtime_timerecord`.`created_date`, 3) = " + str(date_obj.isocalendar()[1]) + " GROUP BY created_date_only ORDER BY NULL")

        # Making a dictionary for the week's records and it's total time
        weeks_start_date = date_obj - timedelta(days=date_obj.weekday())
        previous_week_date = weeks_start_date - timedelta(days=1)

        # next_date will be used inside the loop to increment, we are making the dictionary which
        # contain dates against each day record
        next_date = weeks_start_date
        for x in range(1, 8):
            # Getting the day's data that is in loop from weeks_records
            next_dates_record = list(filter(
                lambda wr: str(wr.created_date_only.day) == str(next_date.day), weeks_records))
            fh.write("next_dates_record: " + str(next_dates_record) + "\n")

            # If we found some data related to the day of the week, we store it in a dictionary,
            # otherwise just store empty array/list
            if len(next_dates_record) > 0:
                new_weekly_dictionary[next_date.strftime('%d-%m-%Y')] = next_dates_record[0]
            else:
                new_weekly_dictionary[next_date.strftime('%d-%m-%Y')] = CustomTimeRecord(
                    total_time_sec=0, created_date_only=next_date)

            # Incrementing the next_date
            next_date = next_date + timedelta(days=1)
        fh.write('new_weekly_dictionary: ' + str(new_weekly_dictionary) + "\n")

        # Summing up total time in the week
        total_time_week = 0
        for week_record in weeks_records:
            total_time_week += week_record.total_time_sec

    except Exception as e:
        fh.write("exception: " + str(e) + "\n")
        # raise e
        records_list = []
        projects = []
        weeks_records = []
        total_time_week = 0

    fh.close()
    # Preparing Context dictionary to be passed to view
    context = {
        'records_list': records_list,
        'projects': projects,
        'weeks_records': weeks_records,
        'total_time_week': total_time_week,
        'daily_date': date_obj,
        'previous_week_date': previous_week_date,
        'next_week_date': weeks_start_date + timedelta(days=7),
        'new_weekly_dictionary': new_weekly_dictionary
    }
    # return HttpResponse(template.render(context, request))
    return render(request, 'logtime/daily.html', context)


def index(request):
    """Index page for just redirecting to daily page."""
    return redirect('daily')
    # raise Http404("Page doesn't exist, please add a view file for it")
