## Django related
# Start dev server
`$ ./manage.py runserver 127.0.0.1:8001`

## Some useful links
_These links can help in development work_

https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest#Submitting_forms_and_uploading_files
https://alyssa.is/pushstate-jquery/
https://developer.mozilla.org/en-US/docs/Web/API/History_API
